
async function fetchCompanies() {
    let result= await fetch(`${API_URL}/companies`);
    let companies = await result.json();
    return companies;
}
async function fetchCompany(companyId) {
    let result= await fetch(`${API_URL}/companies/${companyId}`);
    let company = await result.json();
    return company;
}

async function deleteCompany(companyId){
    await fetch(`${API_URL}/companies/${companyId}`,{
        method:'DELETE'
    });
}
async function saveCompany(company) {
    await fetch(`${API_URL}/companies`,{
        method:'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify(company)
    });

}

