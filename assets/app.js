let currentCompanyId =0;
// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {

    // Function calls for initial page loading activities...
    doLoadCompanies()
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLoadCompanies() {
    let companies= await fetchCompanies();
    console.log('Loading companies...');
    console.log(companies);
    let companiesDiv= document.querySelector('#companiesList');
    let htmlContent= '';

    for(let company of companies) {
        htmlContent= htmlContent+''+/*html*/`
        <div class="company-details">
        <div class="detail-title">Ettevõtte nimi:</div>
        <div class="detail-value">${company.name}</div>
        <div class="detail-title"> Ettevõtte logo:</div>
        <div class="detail-value"><img src="${company.logo}">
        </div>
        <div>
            <button class="button-red"onclick="doDeleteCompany(${company.id})"> Kustuta </button>
            <button onclick= "doOpenPopup(${company.id})">Muuda</button>
    
        </div>
        </div>
        <hr>
        `;
    }

    companiesDiv.innerHTML= htmlContent;
}

async function doDeleteCompany(companyId) {
    if(confirm('Soovid kustutada ettevõtte?')){
        await deleteCompany(companyId);
        doLoadCompanies();

    }}

async function doOpenPopup(companyId) {
    await openPopup(POPUP_CONF_300_300,'companyEditFormTemplate');

    if(companyId>0) {
        console.log('Modify');
        let company= await fetchCompany(companyId);
        console.log(company);
        let nameTextBox = document.querySelector('#name');
        let logoTextBox = document.querySelector('#logo');

        nameTextBox.value= company.name;
        logoTextBox.value= company.logo;
        currentCompanyId= company.id;

    } else {
        console.log('Adding company..');
        currentCompanyId=0;

    }

       
    }

    async function doSaveCompany(){
        console.log('Saving company');
        let nameTextBox = document.querySelector('#name');
        let logoTextBox = document.querySelector('#logo');
        

        let companyName = nameTextBox.value;
        let companyLogo = logoTextBox.value;
        


        let company= {
            id: currentCompanyId,
            name: companyName,
            logo: companyLogo

        };
        console.log(company);
        await saveCompany(company);
        await doLoadCompanies();
        await closePopup();

    }
    
    

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayCompanies(companies) {

}

async function displayCompanyEditPopup(id) {
    await openPopup(POPUP_CONF_DEFAULT, 'companyEditFormTemplate');
}
